export const getErrorMessage = (error) => {
  return error?.response
    ? error?.response?.data?.message
    : "Server Error, please try again later.";
};

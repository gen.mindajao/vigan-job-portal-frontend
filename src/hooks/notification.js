import { useQuasar } from 'quasar'

export default function useNotification () {
	const $q = useQuasar()

	const showNotif = (config) => {
		$q.notify(config)
	}

	return {
		showNotif
	}
}

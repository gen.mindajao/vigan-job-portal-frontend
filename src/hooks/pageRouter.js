import { useRoute, useRouter } from "vue-router";

export default function usePageRouter() {
  const route = useRoute();
  const router = useRouter();

  const setToQuery = (query) => {
    router.push({
      query: {
        ...route.query,
        ...query,
      },
    });
  };

  const doRefresh = () => {
    router.go();
  };

  return {
    route,
    router,
    setToQuery,
    doRefresh,
  };
}

import { api } from "boot/axios";

const resource = "/api/industries";

export default {
  get(payload) {
    return api.get(resource, payload);
  },
};

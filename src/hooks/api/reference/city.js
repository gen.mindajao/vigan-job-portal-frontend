import { api } from "boot/axios";

const resource = "/api/cities";

export default {
  get(payload) {
    return api.get(resource, payload);
  },
};

import { api } from "boot/axios";

const resource = "/api/education-levels";

export default {
  get(payload) {
    return api.get(resource, payload);
  },
};

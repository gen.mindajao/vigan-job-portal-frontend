import { api } from "boot/axios";

const resource = "/api/barangays";

export default {
  get(payload) {
    return api.get(resource, payload);
  },
};

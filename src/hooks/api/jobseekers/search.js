import { api } from "boot/axios";

const resource = "/api/job-seekers/searches";

export default {
  get(payload) {
    return api.get(resource, payload);
  },
};

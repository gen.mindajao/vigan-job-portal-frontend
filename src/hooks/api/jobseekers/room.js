import { api } from "boot/axios";

const resource = "/api/job-seekers/rooms";

export default {
  getIDs() {
    return api.get(`${resource}/ids`);
  },

  get(params) {
    return api.get(`${resource}?` + params);
    // return api.get(`${resource}`, payload);
  },

  show(id, payload) {
    return api.get(`${resource}/${id}`, payload);
  },
};

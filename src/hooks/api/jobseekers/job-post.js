import { api } from "boot/axios";

const resource = "/api/job-seekers/job-posts";

export default {
  get(params) {
    return api.get(`${resource}?` + params);
  },

  show(id) {
    return api.get(`${resource}/${id}`);
  },
};

import { api } from "boot/axios";

const resource = "/api/job-seekers/character-references";

export default {
  create(payload) {
    return api.post(`${resource}`, payload);
  },

  update(id, payload) {
    return api.put(`${resource}/${id}`, payload);
  },

  get() {
    return api.get(`${resource}`);
  },

  show(id) {
    return api.get(`${resource}/${id}`);
  },

  delete(id) {
    return api.delete(`${resource}/${id}`);
  },
};

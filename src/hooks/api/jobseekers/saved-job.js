import { api } from "boot/axios";

const resource = "/api/job-seekers/saved-jobs";

export default {
  get(params) {
    return api.get(`${resource}?` + params);
  },

  post(id) {
    return api.post(`${resource}`, { job_post_id: id });
  },

  delete(id) {
    return api.delete(`${resource}/${id}`);
  },
};

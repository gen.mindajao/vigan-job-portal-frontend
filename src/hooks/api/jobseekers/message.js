import { api } from "boot/axios";

const resource = "/api/job-seekers/messages";

export default {
  create(payload) {
    return api.post(`${resource}`, payload);
  },
};

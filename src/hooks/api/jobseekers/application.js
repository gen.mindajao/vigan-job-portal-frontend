import { api } from "boot/axios";

const resource = "/api/job-seekers/applications";

export default {
  create(payload) {
    return api.post(`${resource}`, payload);
  },

  get(payload) {
    return api.get(`${resource}`, payload);
  },

  show(id) {
    return api.get(`${resource}/${id}`);
  },
};

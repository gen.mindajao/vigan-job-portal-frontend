import { api } from "boot/axios";

const resource = "/api/job-seekers/users";

export default {
  show(id, payload) {
    return api.get(`${resource}/${id}`, payload);
  },

  update(id, payload) {
    return api.put(`${resource}/${id}`, payload);
  },

  updateResume(id, payload) {
    return api.post(`${resource}/cvs-or-resumes/${id}`, payload);
  },

  updatePhoto(id, payload) {
    return api.post(`${resource}/profile-pictures/${id}`, payload);
  },

  updatePassword(id, payload) {
    return api.put(`${resource}/passwords/${id}`, payload);
  },
};

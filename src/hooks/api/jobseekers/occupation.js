import { api } from "boot/axios";

const resource = "/api/job-seekers";

export default {
  create(payload) {
    return api.post(`${resource}/occupations`, payload);
  },
};

import { api } from "boot/axios";

const resource = "/api/forgot-password";

export default {
  sendCode(payload) {
    return api.post(`${resource}/send`, payload);
  },

  verifyCode(payload) {
    return api.post(`${resource}/verify`, payload);
  },
};

import { api } from "boot/axios";

const resource = "/api/address";

export default {
  create(id, payload) {
    return api.post(`${resource}/users/${id}`, payload);
  },

  show(id) {
    return api.get(`${resource}/${id}`);
  },

  update(id, payload) {
    return api.put(`${resource}/${id}`, payload);
  },
};

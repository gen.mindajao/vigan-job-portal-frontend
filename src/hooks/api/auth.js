import { api } from "boot/axios";

const resource = "/api";

export default {
  login(payload) {
    return api.post(`${resource}/login`, payload);
  },

  sendCode(payload) {
    return api.post(`${resource}/setup/job-seekers`, payload);
  },

  registerEmployer(payload) {
    return api.post(`${resource}/register/employers`, payload);
  },

  registerJobseeker(payload) {
    return api.post(`${resource}/register/job-seekers`, payload);
  },

  verifyEmail(payload) {
    return api.post(`${resource}/users/verify`, payload);
  },

  resendCode(payload) {
    return api.post(`${resource}/users/verify/resend`, payload);
  },

  checkEmail(payload) {
    return api.post(`${resource}/setup/check-emails`, payload);
  },
};

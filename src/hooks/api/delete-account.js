import { api } from "boot/axios";

const resource = "/api/account-deletion-requests";

export default {
  request() {
    return api.post(`${resource}`);
  },
};

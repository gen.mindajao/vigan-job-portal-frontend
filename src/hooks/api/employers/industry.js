import { api } from "boot/axios";

const resource = "/api/employers/industries";

export default {
  create(payload) {
    return api.post(`${resource}`, payload);
  },

  get(payload) {
    return api.get(`${resource}`, payload);
  },

  show(id) {
    return api.get(`${resource}/${id}`);
  },

  delete(id) {
    return api.delete(`${resource}/${id}`);
  },
};

import { api } from "boot/axios";

const resource = "/api/employers/job-posts/unread-count";

export default {
  get() {
    return api.get(`${resource}`);
  },
};

import { api } from "boot/axios";

const resource = "/api/employers/users";

export default {
  update(id, payload) {
    return api.post(`${resource}/${id}`, payload);
  },

  updatePhoto(id, payload) {
    return api.post(`${resource}/profile-pictures/${id}`, payload);
  },

  updatePassword(id, payload) {
    return api.put(`${resource}/passwords/${id}`, payload);
  },

  show(id) {
    const payload = {
      _method: "PUT",
    };
    return api.get(`${resource}/${id}`, payload);
  },
};

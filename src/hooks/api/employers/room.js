import { api } from "boot/axios";

const resource = "/api/employers/rooms";

export default {
  getIDs() {
    return api.get(`${resource}/ids`);
  },

  get() {
    return api.get(`${resource}`);
  },

  show(id, payload) {
    return api.get(`${resource}/${id}`, payload);
  },
};

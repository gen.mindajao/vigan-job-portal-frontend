import { api } from "boot/axios";

const resource = "/api/employers/requirements";

export default {
  create(payload) {
    return api.post(`${resource}`, payload);
  },

  update(id, payload) {
    return api.post(`${resource}/${id}`, payload);
  },

  get(payload) {
    return api.get(`${resource}`, payload);
  },

  show(id) {
    return api.get(`${resource}/${id}`);
  },
};

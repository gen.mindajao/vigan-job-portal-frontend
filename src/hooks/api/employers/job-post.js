import { api } from "boot/axios";

const resource = "/api/employers/job-posts";

export default {
  create(payload) {
    return api.post(`${resource}`, payload);
  },

  get(payload) {
    return api.get(`${resource}`, payload);
  },

  show(id) {
    return api.get(`${resource}/${id}`);
  },

  archive(id) {
    const payload = {
      is_archived: true,
    };
    return api.put(`${resource}/archive/${id}`, payload);
  },
};

import { api } from "boot/axios";

const resource = "/api/employers/applications";

export default {
  show(id) {
    return api.get(`${resource}/${id}`);
  },

  update(id, payload = {}) {
    return api.put(`${resource}/${id}`, payload);
  },
};

import { LocalStorage } from 'quasar'
import { computed } from 'vue'
import { useRoute } from 'vue-router'

export default function usePagePermission () {
  const route = useRoute()

  const routeName = computed(() => {
    const userRole = LocalStorage.getItem('role') || 'default'
    const viewFor = route.meta.viewFor

    if (route.meta.forceDefault) {
      return 'default'
    }

    if (
      userRole &&
      (Array.isArray(viewFor) && viewFor.includes(userRole))
    ) {
      return userRole
    }

    return 'default'
  })

  const isAuthorized = computed(() => {
    const userRole = LocalStorage.getItem('role') || 'default'
    const viewFor = route.meta.viewFor

    if (route.meta.forceDefault) {
      return true
    }

    if (
      userRole &&
      (Array.isArray(viewFor) && viewFor.includes(userRole))
    ) {
      return true
    }

    return false
  })

  return {
    routeName,
    isAuthorized
  }
}

export default function useParsing () {
  const numberToFinancial = (
		{
			value = 0,
			delimiter = ',',
			addDecimalPlace = false,
			decimalPlaceCount = 2
		}
	) => {
		let toNumber = Number(value) || 0

		if (toNumber) {
			if (addDecimalPlace) {
				toNumber = toNumber.toFixed(decimalPlaceCount)
			}

			// return toNumber.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, delimiter)

			/**
			 * Error: Invalid regular expression: invalid group specifier name
			 * To avoid Safari's lookbehind
			 */
			const parts = toNumber.toString().split('.')
			return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, delimiter) + (parts[1] ? '.' + parts[1] : '')
		}

		return toNumber.toFixed(decimalPlaceCount)
	}

	const showValue = (value, forEmptyValue = null) => {
		if (value) {
			return value
		}

		return forEmptyValue
	}

	return {
		numberToFinancial,
		showValue
	}
}

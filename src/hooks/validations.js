export const requiredField = (v) => !!v || "This field is required";
// export const emailFormat = (v) => /.+@.+.com/.test(v) || "Email must be valid";
export const emailFormat = (v) =>
  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(v) ||
  "Email must be valid";

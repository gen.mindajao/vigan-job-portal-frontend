import {
  MY_JOBS_EMPLOYER_BASE_PATH,
  MY_JOBS_EMPLOYER_ROUTE_NAMES,
} from "../constants";

export const MyJobsEmployerModuleRoutes = [
  {
    path: MY_JOBS_EMPLOYER_BASE_PATH,
    name: "MyJobsEmployerModule",
    component: () =>
      import(
        /* webpackChunkName: "MyJobsEmployerModule" */ "../MyJobsEmployerModule.vue"
      ),
    redirect: MY_JOBS_EMPLOYER_ROUTE_NAMES.MyJobsEmployer.link,
    meta: {
      requiredAuth: true,
    },
    children: [
      {
        path: MY_JOBS_EMPLOYER_ROUTE_NAMES.MyJobsEmployer.link,
        name: MY_JOBS_EMPLOYER_ROUTE_NAMES.MyJobsEmployer.name,
        components: {
          default: () =>
            import(
              /* webpackChunkName: "MyJobsEmployerPage" */ `../pages/${MY_JOBS_EMPLOYER_ROUTE_NAMES.MyJobsEmployer.name}.vue`
            ),
        },
        meta: {
          requiredAuth: true,
          viewFor: [],
          forceDefault: false,
        },
      },
      {
        path: MY_JOBS_EMPLOYER_ROUTE_NAMES.JobDetails.link,
        name: MY_JOBS_EMPLOYER_ROUTE_NAMES.JobDetails.name,
        components: {
          default: () =>
            import(
              /* webpackChunkName: "JobDetailsPage" */ `../pages/${MY_JOBS_EMPLOYER_ROUTE_NAMES.JobDetails.name}.vue`
            ),
        },
        meta: {
          requiredAuth: true,
          viewFor: [],
          forceDefault: false,
        },
      },
    ],
  },
];

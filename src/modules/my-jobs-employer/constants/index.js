import useParsing from "src/hooks/parsing";
const { showValue } = useParsing();

export const MY_JOBS_EMPLOYER_BASE_PATH = "/my-jobs-employer";

export const MY_JOBS_EMPLOYER_ROUTE_NAMES = {
  MyJobsEmployer: {
    name: "MyJobsEmployerPage",
    link: `${MY_JOBS_EMPLOYER_BASE_PATH}/`,
  },
  JobDetails: {
    name: "JobDetailsPage",
    link: `${MY_JOBS_EMPLOYER_BASE_PATH}/job-details`,
  },
};

export const ARCHIVE_OPTIONS = [
  {
    name: "Position Filled",
    value: "filled",
    separator: true,
    color: "white",
  },
  {
    name: "Deactivate without hiring",
    value: "deactivated_without_hiring",
    color: "white",
  },
];

export const TABLES = {
  ActiveJobsTable: {
    columns: [
      {
        name: "new",
        align: "center",
        label: "",
        field: "new",
        sortable: false,
      },
      {
        name: "id",
        align: "left",
        label: "JOB ID NO.",
        field: "id",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "job_title",
        align: "center",
        label: "JOB TITLE",
        field: "job_title",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "job_post_validity",
        align: "center",
        label: "JOB POST VALIDITY",
        field: "job_post_validity",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "applications_count",
        align: "center",
        label: "NO. OF APPLICANTS",
        field: "applications_count",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "actions",
        align: "center",
        label: "",
        field: "actions",
        sortable: false,
      },
    ],
  },

  PendingJobsTable: {
    columns: [
      {
        name: "new",
        align: "center",
        label: "",
        field: "new",
        sortable: false,
      },
      {
        name: "id",
        align: "left",
        label: "JOB ID NO.",
        field: "id",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "job_title",
        align: "center",
        label: "JOB TITLE",
        field: "job_title",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "created_at",
        align: "center",
        label: "SUBMITTED ON",
        field: "created_at",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "status",
        align: "center",
        label: "STATUS",
        field: "status",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "actions",
        align: "center",
        label: "",
        field: "actions",
        sortable: false,
      },
    ],
  },

  ArchivedJobsTable: {
    columns: [
      {
        name: "new",
        align: "center",
        label: "",
        field: "new",
        sortable: false,
      },
      {
        name: "id",
        align: "left",
        label: "JOB ID NO.",
        field: "id",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "job_title",
        align: "center",
        label: "JOB TITLE",
        field: "job_title",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "created_at",
        align: "center",
        label: "POSTED ON",
        field: "created_at",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "applications_count",
        align: "center",
        label: "NO. OF APPLICANTS",
        field: "applications_count",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "actions",
        align: "center",
        label: "",
        field: "actions",
        sortable: false,
      },
    ],
  },

  JobDetailsTable: {
    columns: [
      {
        name: "reference_no",
        align: "center",
        label: "REF NO.",
        field: "reference_no",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "first_name",
        align: "center",
        label: "FIRST NAME",
        field: "first_name",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "last_name",
        align: "center",
        label: "LAST NAME",
        field: "last_name",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "gender",
        align: "center",
        label: "GENDER",
        field: "gender",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "age",
        align: "center",
        label: "AGE",
        field: "age",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "barangay",
        align: "center",
        label: "BARANGAY",
        field: "barangay",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "status",
        align: "center",
        label: "STATUS",
        field: "status",
        sortable: false,
        format: (val) => showValue(val, "-"),
      },
      {
        name: "actions",
        align: "center",
        label: "",
        field: "actions",
        sortable: false,
      },
    ],
  },
};

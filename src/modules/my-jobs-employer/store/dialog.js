import { defineStore } from "pinia";

const defaultState = () => ({
  showArchiveJob: false,
});

export const useMyJobsDialog = defineStore("dialog", {
  state: defaultState,

  getters: {},

  actions: {
    showArchiveJobDialog() {
      this.showArchiveJob = !this.showArchiveJob;
    },
  },
});

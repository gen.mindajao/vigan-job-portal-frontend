import { defineStore } from "pinia";
import { EmployerJobPostApi } from "src/hooks/api";

const defaultState = () => ({
  activeJobs: [],
  activeJob: null,
  pendingJobs: [],
  pendingJob: null,
  archivedJobs: [],
  archivedJob: null,
  jobDetails: [],
  jobDetail: null,
  applications: [],
  activeLoading: false,
  pendingLoading: false,
  archivedLoading: false,
  jobLoading: false,
});

export const useJob = defineStore("job", {
  state: defaultState,

  getters: {},

  actions: {
    async getActiveJobs(payload) {
      const params = payload;

      this.activeLoading = true;
      await EmployerJobPostApi.get({ params })
        .then((response) => {
          this.activeLoading = false;
          this.activeJobs = response.data.data.filter(
            (item) => item.status === "approved" && item.is_archived === false
          );
        })
        .catch(() => {
          this.activeJobs = [];
        });
    },

    async getPendingJobs(payload) {
      const params = payload;

      this.pendingLoading = true;
      await EmployerJobPostApi.get({ params })
        .then((response) => {
          this.pendingLoading = false;
          this.pendingJobs = response.data.data.filter(
            (item) => item.status === "pending"
          );
        })
        .catch(() => {
          this.pendingJobs = [];
        });
    },

    async getArchivedJobs(payload) {
      const params = payload;

      this.archivedLoading = true;
      await EmployerJobPostApi.get({ params })
        .then((response) => {
          this.archivedLoading = false;
          this.archivedJobs = response.data.data.filter(
            (item) => item.is_archived === true
          );
        })
        .catch(() => {
          this.archivedJobs = [];
        });
    },

    async showJobDetails(id) {
      this.jobDetail = {};
      this.applications = [];
      this.jobLoading = true;
      await EmployerJobPostApi.show(id)
        .then((response) => {
          this.jobLoading = false;
          this.jobDetail = response.data.data;
          this.applications = response.data.data.applications;
        })
        .catch(() => {
          this.jobDetail = {};
          this.applications = [];
        });
    },
  },
});

import { ref } from "vue";
import { EmployerJobPostApi } from "src/hooks/api";
import useNotification from "src/hooks/notification";

export default function useJobHook() {
  const loading = ref(false);
  const { showNotif } = useNotification();

  const doCreateJobPost = async (payload) => {
    loading.value = true;

    await EmployerJobPostApi.create(payload)
      .then(() => {
        showNotif({
          message:
            "You job post has been submitted for review by the PESO admin. Once the job post is approved you will be notified.",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doArchiveJobPost = async (id, payload = {}) => {
    loading.value = true;

    await EmployerJobPostApi.archive(id, payload)
      .then(() => {
        showNotif({
          message: "You job post has been archived",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  return {
    loading,
    doCreateJobPost,
    doArchiveJobPost,
  };
}

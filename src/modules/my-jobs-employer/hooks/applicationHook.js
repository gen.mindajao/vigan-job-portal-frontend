import { ref } from "vue";
import { EmployerApplicationApi } from "src/hooks/api";
import useNotification from "src/hooks/notification";

export default function useApplicationHook() {
  const loading = ref(false);
  const { showNotif } = useNotification();

  const doUpdateApplicationStatus = async (id, payload = {}) => {
    loading.value = true;

    await EmployerApplicationApi.update(id, payload)
      .then(() => {
        showNotif({
          message: "You have successfully updated the applicant's status",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  return {
    loading,
    doUpdateApplicationStatus,
  };
}

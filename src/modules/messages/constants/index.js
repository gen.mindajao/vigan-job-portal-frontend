export const MESSAGES_PATH = "/messages";

export const MESSAGES_ROUTE_NAMES = {
  Messages: {
    name: "MessagesPage",
    link: `${MESSAGES_PATH}/`,
  },
};

import { MESSAGES_PATH, MESSAGES_ROUTE_NAMES } from "../constants";

export const MessagesModuleRoutes = [
  {
    path: MESSAGES_PATH,
    name: "MessagesModule",
    component: () =>
      import(/* webpackChunkName: "MessagesModule" */ "../MessagesModule.vue"),
    redirect: MESSAGES_ROUTE_NAMES.Messages.link,
    meta: {
      requiredAuth: true,
    },
    children: [
      {
        path: MESSAGES_ROUTE_NAMES.Messages.link,
        name: MESSAGES_ROUTE_NAMES.Messages.name,
        components: {
          default: () =>
            import(
              /* webpackChunkName: "MessagesPage" */ `../pages/${MESSAGES_ROUTE_NAMES.Messages.name}.vue`
            ),
        },
        meta: {
          requiredAuth: true,
          viewFor: [],
          forceDefault: false,
        },
      },
    ],
  },
];

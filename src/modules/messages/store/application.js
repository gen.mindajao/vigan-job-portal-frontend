import { defineStore } from "pinia";
import { JobseekerApplicationApi } from "src/hooks/api";
import { LocalStorage } from "quasar";

const defaultState = () => ({
  applications: [],
  application: null,
  currentApplicationStatusID: null,
  selectedApplicationID: null,
});

export const useApplication = defineStore(
  "applications_" + LocalStorage.getItem("auth").id,
  {
    state: defaultState,

    getters: {},

    actions: {
      async getApplications() {
        await JobseekerApplicationApi.get()
          .then((response) => {
            this.applications = response.data.data;
          })
          .catch(() => {
            this.applications = [];
          });
      },

      async showApplication(id) {
        return await JobseekerApplicationApi.show(id)
          .then((response) => {
            this.application = response.data.data;
            this.selectedApplicationID = id;
            this.currentApplicationStatusID =
              response.data.data.application_status_id;
            return response.data;
          })
          .catch(() => {
            this.application = {};
          });
      },

      setApplicationDetail(application) {
        this.application = application;
        this.selectedApplicationID = application.id;
        this.currentApplicationStatusID = application.application_status_id;
      },
    },
  }
);

import { defineStore } from "pinia";
import { JobseekerJobPostApi } from "src/hooks/api";
import { LocalStorage } from "quasar";

const defaultState = () => ({
  jobs: [],
  job: null,
});

export const useJob = defineStore("jobs_" + LocalStorage.getItem("auth").id, {
  state: defaultState,

  getters: {},

  actions: {
    async getJobs() {
      await JobseekerJobPostApi.get()
        .then((response) => {
          this.jobs = response.data.data;
        })
        .catch(() => {
          this.jobs = [];
        });
    },

    async showJob(id) {
      await JobseekerJobPostApi.show(id)
        .then((response) => {
          this.job = response.data.data;
        })
        .catch(() => {
          this.job = null;
        });
    },
  },
});

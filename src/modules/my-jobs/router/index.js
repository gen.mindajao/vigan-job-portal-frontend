import { MY_JOBS_BASE_PATH, MY_JOBS_ROUTE_NAMES } from "../constants";

export const MyJobsModuleRoutes = [
  {
    path: MY_JOBS_BASE_PATH,
    name: "MyJobsModule",
    component: () =>
      import(/* webpackChunkName: "MyJobsModule" */ "../MyJobsModule.vue"),
    redirect: MY_JOBS_ROUTE_NAMES.MyJobs.link,
    meta: {
      requiredAuth: true,
    },
    children: [
      {
        path: MY_JOBS_ROUTE_NAMES.MyJobs.link,
        name: MY_JOBS_ROUTE_NAMES.MyJobs.name,
        components: {
          default: () =>
            import(
              /* webpackChunkName: "MyJobsPage" */ `../pages/${MY_JOBS_ROUTE_NAMES.MyJobs.name}.vue`
            ),
        },
        meta: {
          requiredAuth: true,
          viewFor: [],
          forceDefault: false,
        },
      },
    ],
  },
];

export const MY_JOBS_BASE_PATH = "/my-jobs";

export const MY_JOBS_ROUTE_NAMES = {
  MyJobs: {
    name: "MyJobsPage",
    link: `${MY_JOBS_BASE_PATH}/`,
  },
};

import { defineStore } from "pinia";
import { JobseekerJobPostApi } from "src/hooks/api";

const defaultState = () => ({
  jobs: [],
  job: null,
});

export const useJobsList = defineStore("job", {
  state: defaultState,

  getters: {},

  actions: {
    async getJobs(payload) {
      const params = payload;

      await JobseekerJobPostApi.get({ params })
        .then((response) => {
          this.jobs = response.data.data;
        })
        .catch(() => {
          this.jobs = [];
        });
    },

    async getJob(id) {
      await JobseekerJobPostApi.show(id)
        .then((response) => {
          this.job = response.data.data;
        })
        .catch(() => {
          this.job = {};
        });
    },
  },
});

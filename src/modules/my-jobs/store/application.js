import { defineStore } from "pinia";
import { JobseekerApplicationApi } from "src/hooks/api";
import { LocalStorage } from "quasar";

const defaultState = () => ({
  applications: [],
  application: null,
});

export const useApplicationsList = defineStore(
  "application_" + LocalStorage.getItem("auth").id,
  {
    state: defaultState,

    getters: {},

    actions: {
      async getApplications() {
        await JobseekerApplicationApi.get()
          .then((response) => {
            this.applications = response.data.data;
          })
          .catch(() => {
            this.applications = [];
          });
      },

      async getApplication(id) {
        await JobseekerApplicationApi.show(id)
          .then((response) => {
            this.application = response.data.data;
          })
          .catch(() => {
            this.application = {};
          });
      },
    },
  }
);

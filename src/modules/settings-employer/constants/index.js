export const SETTINGS_EMPLOYER_BASE_PATH = "/settings-employer";

export const SETTINGS_EMPLOYER_ROUTE_NAMES = {
  Settings: {
    name: "SettingsEmployerPage",
    link: `${SETTINGS_EMPLOYER_BASE_PATH}/`,
  },
};

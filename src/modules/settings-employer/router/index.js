import {
  SETTINGS_EMPLOYER_BASE_PATH,
  SETTINGS_EMPLOYER_ROUTE_NAMES,
} from "../constants";

export const SettingsEmployerModuleRoutes = [
  {
    path: SETTINGS_EMPLOYER_BASE_PATH,
    name: "SettingsEmployerModule",
    component: () =>
      import(
        /* webpackChunkName: "SettingsEmployerModule" */ "../SettingsEmployerModule.vue"
      ),
    redirect: SETTINGS_EMPLOYER_ROUTE_NAMES.Settings.link,
    meta: {
      requiredAuth: true,
    },
    children: [
      {
        path: SETTINGS_EMPLOYER_ROUTE_NAMES.Settings.link,
        name: SETTINGS_EMPLOYER_ROUTE_NAMES.Settings.name,
        components: {
          default: () =>
            import(
              /* webpackChunkName: "SettingsEmployerPage" */ `../pages/${SETTINGS_EMPLOYER_ROUTE_NAMES.Settings.name}.vue`
            ),
        },
        meta: {
          requiredAuth: true,
          viewFor: [],
          forceDefault: false,
        },
      },
    ],
  },
];

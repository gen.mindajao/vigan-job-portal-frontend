export const MY_PROFILE_BASE_PATH = "/my-profile";

export const MY_PROFILE_ROUTE_NAMES = {
  MyProfile: {
    name: "MyProfilePage",
    link: `${MY_PROFILE_BASE_PATH}/`,
  },
};

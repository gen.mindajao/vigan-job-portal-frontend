import { defineStore } from "pinia";
import { JobseekerCertificateApi } from "src/hooks/api";

const defaultState = () => ({
  trainings: [],
  training: null,
});

export const useTraining = defineStore("trainings", {
  state: defaultState,

  getters: {},

  actions: {
    async getTrainings() {
      await JobseekerCertificateApi.get()
        .then((response) => {
          this.trainings = response.data.data;
        })
        .catch(() => {
          this.trainings = [];
        });
    },

    async showTraining(id) {
      await JobseekerCertificateApi.show(id)
        .then((response) => {
          this.training = response.data.data;
        })
        .catch(() => {
          this.training = null;
        });
    },
  },
});

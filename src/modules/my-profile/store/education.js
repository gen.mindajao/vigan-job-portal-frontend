import { defineStore } from "pinia";
import { JobseekerEducationApi } from "src/hooks/api";

const defaultState = () => ({
  educations: [],
  education: null,
});

export const useEducation = defineStore("educations", {
  state: defaultState,

  getters: {},

  actions: {
    async getEducations() {
      await JobseekerEducationApi.get()
        .then((response) => {
          this.educations = response.data.data;
        })
        .catch(() => {
          this.educations = [];
        });
    },

    async showEducation(id) {
      await JobseekerEducationApi.show(id)
        .then((response) => {
          this.education = response.data.data;
        })
        .catch(() => {
          this.education = null;
        });
    },
  },
});

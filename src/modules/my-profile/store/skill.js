import { defineStore } from "pinia";
import { JobseekerSkillApi } from "src/hooks/api";

const defaultState = () => ({
  skills: [],
});

export const useSkill = defineStore("skills", {
  state: defaultState,

  getters: {},

  actions: {
    async getMySkills() {
      await JobseekerSkillApi.get()
        .then((response) => {
          this.skills = response.data.data;
        })
        .catch(() => {
          this.skills = [];
        });
    },
  },
});

import { defineStore } from "pinia";
import { JobseeekerCharacterReferenceApi } from "src/hooks/api";

const defaultState = () => ({
  characterReferences: [],
  characterReference: {},
});

export const useCharacterReference = defineStore("references", {
  state: defaultState,

  getters: {},

  actions: {
    async getReferences() {
      await JobseeekerCharacterReferenceApi.get()
        .then((response) => {
          this.characterReferences = response.data.data;
        })
        .catch(() => {
          this.characterReferences = [];
        });
    },

    async showReference(id) {
      await JobseeekerCharacterReferenceApi.show(id)
        .then((response) => {
          this.characterReference = response.data.data;
        })
        .catch(() => {
          this.characterReference = null;
        });
    },
  },
});

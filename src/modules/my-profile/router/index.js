import { MY_PROFILE_BASE_PATH, MY_PROFILE_ROUTE_NAMES } from "../constants";

export const MyProfileModuleRoutes = [
  {
    path: MY_PROFILE_BASE_PATH,
    name: "MyProfileModule",
    component: () =>
      import(
        /* webpackChunkName: "MyProfileModule" */ "../MyProfileModule.vue"
      ),
    redirect: MY_PROFILE_ROUTE_NAMES.MyProfile.link,
    meta: {
      requiredAuth: true,
    },
    children: [
      {
        path: MY_PROFILE_ROUTE_NAMES.MyProfile.link,
        name: MY_PROFILE_ROUTE_NAMES.MyProfile.name,
        components: {
          default: () =>
            import(
              /* webpackChunkName: "MyProfilePage" */ `../pages/${MY_PROFILE_ROUTE_NAMES.MyProfile.name}.vue`
            ),
        },
        meta: {
          requiredAuth: true,
          viewFor: [],
          forceDefault: false,
        },
      },
    ],
  },
];

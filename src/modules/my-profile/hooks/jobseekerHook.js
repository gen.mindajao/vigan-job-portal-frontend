import { ref } from "vue";
import {
  JobseekerUserApi,
  JobseekerCertificateApi,
  JobseekerEducationApi,
  JobseekerSkillApi,
  JobseekerWorkHistoryApi,
  JobseeekerCharacterReferenceApi,
  UserAddressApi,
} from "src/hooks/api";
import useNotification from "src/hooks/notification";
import { LocalStorage } from "quasar";

export default function useJobseekerHook() {
  const loading = ref(false);
  const deleteLoading = ref(false);
  const { showNotif } = useNotification();

  const doUpdateBasicInfo = async (id, payload) => {
    loading.value = true;

    await JobseekerUserApi.update(id, {
      ...payload,
    })
      .then(() => {
        showNotif({
          message: "You have succesfully updated your information",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doUpdatePhoto = async (id, payload) => {
    loading.value = true;

    await JobseekerUserApi.updatePhoto(id, payload)
      .then(() => {
        showNotif({
          message: "You have successfully uploaded your profile photo",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doUpdateResume = async (id, payload) => {
    loading.value = true;

    await JobseekerUserApi.updateResume(id, payload)
      .then(() => {
        showNotif({
          message: "You have successfully uploaded your resume",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doUpdateAddress = async (id, payload) => {
    loading.value = true;

    await UserAddressApi.update(id, {
      ...payload,
    })
      .then((response) => {
        let data = LocalStorage.getItem("auth");
        data.address = response.data.data;
        LocalStorage.set("auth", data);
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doCreateEducation = async (payload) => {
    loading.value = true;

    await JobseekerEducationApi.create(payload)
      .then(() => {
        showNotif({
          message: "You have succesfully added information",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doUpdateEducation = async (id, payload) => {
    loading.value = true;

    await JobseekerEducationApi.update(id, {
      ...payload,
    })
      .then(() => {
        showNotif({
          message: "You have succesfully updated your information",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doDeleteEducation = async (payload) => {
    deleteLoading.value = true;

    await JobseekerEducationApi.delete(payload)
      .then(() => {
        showNotif({
          message: "You have succesfully deleted a record",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        deleteLoading.value = false;
      });
  };

  const doCreateSkill = async (payload) => {
    loading.value = true;

    await JobseekerSkillApi.create(payload)
      .then(() => {
        showNotif({
          message: "You have succesfully updated your information",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doDeleteSkill = async (payload) => {
    deleteLoading.value = true;

    await JobseekerSkillApi.delete(payload)
      .then(() => {
        showNotif({
          message: "You have succesfully deleted a record",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        deleteLoading.value = false;
      });
  };

  const doCreateWork = async (payload) => {
    loading.value = true;

    await JobseekerWorkHistoryApi.create(payload)
      .then(() => {
        showNotif({
          message: "You have succesfully added information",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doUpdateWork = async (id, payload) => {
    loading.value = true;

    await JobseekerWorkHistoryApi.update(id, payload)
      .then(() => {
        showNotif({
          message: "You have succesfully updated your information",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doDeleteWork = async (payload) => {
    deleteLoading.value = true;

    await JobseekerWorkHistoryApi.delete(payload)
      .then(() => {
        showNotif({
          message: "You have succesfully deleted a record",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        deleteLoading.value = false;
      });
  };

  const doCreateTraining = async (payload) => {
    loading.value = true;

    await JobseekerCertificateApi.create(payload)
      .then(() => {
        showNotif({
          message: "You have succesfully added information",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doUpdateTraining = async (id, payload) => {
    loading.value = true;

    await JobseekerCertificateApi.update(id, payload)
      .then(() => {
        showNotif({
          message: "You have succesfully updated your information",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doDeleteTraining = async (payload) => {
    deleteLoading.value = true;

    await JobseekerCertificateApi.delete(payload)
      .then(() => {
        showNotif({
          message: "You have succesfully deleted a record",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        deleteLoading.value = false;
      });
  };

  const doCreateReference = async (payload) => {
    loading.value = true;

    await JobseeekerCharacterReferenceApi.create(payload)
      .then(() => {
        showNotif({
          message: "You have succesfully added information",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doUpdateReference = async (id, payload) => {
    loading.value = true;

    await JobseeekerCharacterReferenceApi.update(id, {
      ...payload,
    })
      .then(() => {
        showNotif({
          message: "You have succesfully updated your information",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doDeleteReference = async (payload) => {
    deleteLoading.value = true;

    await JobseeekerCharacterReferenceApi.delete(payload)
      .then(() => {
        showNotif({
          message: "You have succesfully deleted a record",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        deleteLoading.value = false;
      });
  };

  return {
    loading,
    deleteLoading,
    doUpdateBasicInfo,
    doUpdateAddress,
    doUpdatePhoto,
    doUpdateResume,
    doCreateEducation,
    doUpdateEducation,
    doDeleteEducation,
    doCreateSkill,
    doDeleteSkill,
    doCreateWork,
    doUpdateWork,
    doDeleteWork,
    doCreateTraining,
    doUpdateTraining,
    doDeleteTraining,
    doCreateReference,
    doUpdateReference,
    doDeleteReference,
  };
}

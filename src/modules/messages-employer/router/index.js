import {
  MESSAGES_EMPLOYER_PATH,
  MESSAGES_EMPLOYER_ROUTE_NAMES,
} from "../constants";

export const MessagesEmployerModuleRoutes = [
  {
    path: MESSAGES_EMPLOYER_PATH,
    name: "MessagesEmployerModule",
    component: () =>
      import(
        /* webpackChunkName: "MessagesEmployerModule" */ "../MessagesEmployerModule.vue"
      ),
    redirect: MESSAGES_EMPLOYER_ROUTE_NAMES.Messages.link,
    meta: {
      requiredAuth: true,
    },
    children: [
      {
        path: MESSAGES_EMPLOYER_ROUTE_NAMES.Messages.link,
        name: MESSAGES_EMPLOYER_ROUTE_NAMES.Messages.name,
        components: {
          default: () =>
            import(
              /* webpackChunkName: "MessagesEmployerPage" */ `../pages/${MESSAGES_EMPLOYER_ROUTE_NAMES.Messages.name}.vue`
            ),
        },
        meta: {
          requiredAuth: true,
          viewFor: [],
          forceDefault: false,
        },
      },
      {
        path: MESSAGES_EMPLOYER_ROUTE_NAMES.MessageDetail.link,
        name: MESSAGES_EMPLOYER_ROUTE_NAMES.MessageDetail.name,
        components: {
          default: () =>
            import(
              /* webpackChunkName: "MessagesEmployerPage" */ `../pages/${MESSAGES_EMPLOYER_ROUTE_NAMES.Messages.name}.vue`
            ),
        },
        meta: {
          requiredAuth: true,
          viewFor: [],
          forceDefault: false,
        },
      },
    ],
  },
];

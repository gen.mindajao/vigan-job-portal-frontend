import { defineStore } from "pinia";
import { EmployerUserApi, EmployerRoomApi } from "src/hooks/api";
import { LocalStorage } from "quasar";

const defaultState = () => ({
  roomIDs: [],
  rooms: [],
  room: [],
  selectedRoom: null,
  selectedRoomID: null,
  selectedRoomName: "",
  selectedRoomIndex: null,
  roomsLoading: true,
  messageLoading: true,
});

export const useRoom = defineStore("rooms_" + LocalStorage.getItem("auth").id, {
  state: defaultState,

  getters: {
    roomCount(state) {
      return state.rooms.length;
    },
  },

  actions: {
    async getRoomIDs() {
      console.log("getRoomIDs for Employer");
      await EmployerRoomApi.getIDs()
        .then((response) => {
          this.roomIDs = response.data.data;
          console.log("roomIDs: ", this.roomIDs);
        })
        .catch(() => {
          this.roomIDs = [];
        });
    },

    async getRooms() {
      this.roomsLoading = true;
      console.log("getRooms for Employer");
      await EmployerRoomApi.get()
        .then((response) => {
          this.roomsLoading = false;
          this.rooms = response.data.data;
          this.sortRooms();
        })
        .catch(() => {
          this.rooms = [];
        });
    },

    async showRoom(id, isUnread) {
      this.selectedRoomID = id;

      this.selectedRoomIndex = this.rooms.findIndex((item) => item.id === id);

      this.selectedRoom = this.rooms[this.selectedRoomIndex];
      this.selectedRoomName = this.selectedRoom.room_name;

      if (isUnread) {
        this.selectedRoom.latest_message.read_at = true;
        this.rooms[this.selectedRoomIndex] = this.selectedRoom;
      }

      this.messageLoading = true;
      await EmployerRoomApi.show(id)
        .then((response) => {
          this.messageLoading = false;
          this.room = response.data.data.reverse();
        })
        .catch(() => {
          this.room = [];
        });
    },

    addRoomMessage(message) {
      // console.log("addRoomMessage: ", message);
      let msg = this.room.find((item) => item.id === message.id);
      if (!msg) {
        this.room.push(message);
      }
    },

    updateRoomStatus(status) {
      if (this.selectedRoom != null) {
        this.selectedRoom.application.application_status.name = status;
        this.rooms[this.selectedRoomIndex] = this.selectedRoom;
      }
    },

    resetRooms() {
      this.room = [];
      this.selectedRoom = null;
      this.selectedRoomID = null;
      this.selectedRoomName = "";
      this.selectedRoomIndex = null;
    },

    sortRooms() {
      // let filtered = this.rooms.filter((room) => room.latest_message != null);
      // this.rooms = filtered.sort(
      //   (a, b) =>
      //     new Date(b.latest_message.created_at) -
      //     new Date(a.latest_message.created_at)
      // );
      this.rooms = this.rooms.sort(
        (a, b) => new Date(b.updated_at) - new Date(a.updated_at)
      );
    },

    updateRooms(room) {
      let index = this.rooms.findIndex((item) => item.id === room.id);
      if (index >= 0) {
        let sRoom = this.rooms[index];
        sRoom.latest_message = room.latest_message;

        if (Object.keys(room.application).includes("application_status")) {
          sRoom.application = room.application;
        }

        if (room.id == this.selectedRoomID) {
          sRoom.latest_message.read_at = true;
          this.selectedRoom = sRoom;
        }
        this.rooms[index] = sRoom;
        this.sortRooms();
      } else {
        // this.rooms.push(room);
        this.getRooms();
      }
    },
  },
});

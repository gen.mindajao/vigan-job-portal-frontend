import { defineStore } from "pinia";
import { EmployerApplicationApi } from "src/hooks/api";
import { LocalStorage } from "quasar";

const defaultState = () => ({
  applicant: null,
  applications: [],
  application: null,
  currentApplicationStatusID: 0,
  selectedApplicationID: null,
});

export const useApplication = defineStore(
  "applications_" + LocalStorage.getItem("auth").id,
  {
    state: defaultState,

    getters: {},

    actions: {
      async getApplications() {
        await EmployerApplicationApi.get()
          .then((response) => {
            this.applications = response.data.data;
          })
          .catch(() => {
            this.applications = [];
          });
      },

      async showApplication(id) {
        return await EmployerApplicationApi.show(id)
          .then((response) => {
            console.log("response: ", response);
            this.applicant = response.data.data.user;
            this.application = response.data.data;
            this.selectedApplicationID = id;
            this.currentApplicationStatusID =
              response.data.data.application_status_id;
            return response.data;
          })
          .catch(() => {
            this.application = {};
          });
      },

      setApplicationDetail(application) {
        this.application = application;
        this.selectedApplicationID = application.id;
        this.currentApplicationStatusID = application.application_status_id;
      },
    },
  }
);

import { ref } from "vue";
import { EmployerApplicationApi } from "src/hooks/api";
import useNotification from "src/hooks/notification";

export default function useApplicationHook() {
  const loading = ref(false);
  const { showNotif } = useNotification();

  const doUpdateApplicationStatus = async (id, payload = {}) => {
    loading.value = true;

    return await EmployerApplicationApi.update(id, payload)
      .then((response) => {
        showNotif({
          message: "You have successfully updated the applicant's status",
          color: "positive",
          position: "bottom-right",
        });
        return response.data;
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
        return error?.response?.data;
      })
      .finally(() => {
        loading.value = false;
      });
  };

  return {
    loading,
    doUpdateApplicationStatus,
  };
}

export const MESSAGES_EMPLOYER_PATH = "/messages-employer";

export const MESSAGES_EMPLOYER_ROUTE_NAMES = {
  Messages: {
    name: "MessagesEmployerPage",
    link: `${MESSAGES_EMPLOYER_PATH}/`,
  },
  MessageDetail: {
    name: "MessagesDetailPage",
    link: `${MESSAGES_EMPLOYER_PATH}/:roomid`,
  },
};

import { ref } from "vue";
import {
  JobseekerApplicationApi,
  JobseekerSavedJobPostApi,
} from "src/hooks/api";
import useNotification from "src/hooks/notification";

export default function useApplicationHook() {
  const loading = ref(false);
  const { showNotif } = useNotification();

  const doApplyJob = async (payload) => {
    loading.value = true;

    await JobseekerApplicationApi.create(payload)
      .then(() => {
        showNotif({
          message: "You have successfully applied to this job.",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doSaveJob = async (id) => {
    loading.value = true;

    await JobseekerSavedJobPostApi.post(id)
      .then(() => {
        showNotif({
          message: "You have successfully saved this job.",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doUnsaveJob = async (id) => {
    loading.value = true;

    await JobseekerSavedJobPostApi.delete(id)
      .then(() => {
        showNotif({
          message: "You have unsaved this job.",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  return {
    loading,
    doApplyJob,
    doSaveJob,
    doUnsaveJob,
  };
}

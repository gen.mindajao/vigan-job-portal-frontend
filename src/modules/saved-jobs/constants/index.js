export const SAVED_JOBS_BASE_PATH = "/saved-jobs";

export const SAVED_JOBS_ROUTE_NAMES = {
  SavedJobs: {
    name: "SavedJobsPage",
    link: `${SAVED_JOBS_BASE_PATH}/`,
  },
};

import { SAVED_JOBS_BASE_PATH, SAVED_JOBS_ROUTE_NAMES } from "../constants";

export const SavedJobsModuleRoutes = [
  {
    path: SAVED_JOBS_BASE_PATH,
    name: "SavedJobsModule",
    component: () =>
      import(
        /* webpackChunkName: "SavedJobsModule" */ "../SavedJobsModule.vue"
      ),
    redirect: SAVED_JOBS_ROUTE_NAMES.SavedJobs.link,
    meta: {
      requiredAuth: true,
    },
    children: [
      {
        path: SAVED_JOBS_ROUTE_NAMES.SavedJobs.link,
        name: SAVED_JOBS_ROUTE_NAMES.SavedJobs.name,
        components: {
          default: () =>
            import(
              /* webpackChunkName: "SavedJobsPage" */ `../pages/${SAVED_JOBS_ROUTE_NAMES.SavedJobs.name}.vue`
            ),
        },
        meta: {
          requiredAuth: true,
          viewFor: [],
          forceDefault: false,
        },
      },
    ],
  },
];

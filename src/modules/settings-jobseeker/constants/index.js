export const SETTINGS_JOBSEEKER_BASE_PATH = "/settings-jobseeker";

export const SETTINGS_JOBSEEKER_ROUTE_NAMES = {
  Settings: {
    name: "SettingsJobseekerPage",
    link: `${SETTINGS_JOBSEEKER_BASE_PATH}/`,
  },
};

import {
  SETTINGS_JOBSEEKER_BASE_PATH,
  SETTINGS_JOBSEEKER_ROUTE_NAMES,
} from "../constants";

export const SettingsJobseekerModuleRoutes = [
  {
    path: SETTINGS_JOBSEEKER_BASE_PATH,
    name: "SettingsJobseekerModule",
    component: () =>
      import(
        /* webpackChunkName: "SettingsJobseekerModule" */ "../SettingsJobseekerModule.vue"
      ),
    redirect: SETTINGS_JOBSEEKER_ROUTE_NAMES.Settings.link,
    meta: {
      requiredAuth: true,
    },
    children: [
      {
        path: SETTINGS_JOBSEEKER_ROUTE_NAMES.Settings.link,
        name: SETTINGS_JOBSEEKER_ROUTE_NAMES.Settings.name,
        components: {
          default: () =>
            import(
              /* webpackChunkName: "SettingsJobseekerPage" */ `../pages/${SETTINGS_JOBSEEKER_ROUTE_NAMES.Settings.name}.vue`
            ),
        },
        meta: {
          requiredAuth: true,
          viewFor: [],
          forceDefault: false,
        },
      },
    ],
  },
];

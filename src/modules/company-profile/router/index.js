import {
  COMPANY_PROFILE_BASE_PATH,
  COMPANY_PROFILE_ROUTE_NAMES,
} from "../constants";

export const CompanyProfileModuleRoutes = [
  {
    path: COMPANY_PROFILE_BASE_PATH,
    name: "CompanyProfileModule",
    component: () =>
      import(
        /* webpackChunkName: "CompanyProfileModule" */ "../CompanyProfileModule.vue"
      ),
    redirect: COMPANY_PROFILE_ROUTE_NAMES.CompanyProfile.link,
    meta: {
      requiredAuth: true,
    },
    children: [
      {
        path: COMPANY_PROFILE_ROUTE_NAMES.CompanyProfile.link,
        name: COMPANY_PROFILE_ROUTE_NAMES.CompanyProfile.name,
        components: {
          default: () =>
            import(
              /* webpackChunkName: "CompanyProfilePage" */ `../pages/${COMPANY_PROFILE_ROUTE_NAMES.CompanyProfile.name}.vue`
            ),
        },
        meta: {
          requiredAuth: true,
          viewFor: [],
          forceDefault: false,
        },
      },
    ],
  },
];

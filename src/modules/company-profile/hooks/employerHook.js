import { ref } from "vue";
import {
  EmployerUserApi,
  EmployerRequirementApi,
  UserAddressApi,
} from "src/hooks/api";
import useNotification from "src/hooks/notification";

export default function useEmployerHook() {
  const loading = ref(false);
  const deleteLoading = ref(false);
  const { showNotif } = useNotification();

  const doUpdateBasicInfo = async (id, payload) => {
    loading.value = true;

    await EmployerUserApi.update(id, {
      ...payload,
    })
      .then(() => {
        showNotif({
          message: "You have succesfully updated your information",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doUpdatePhoto = async (id, payload) => {
    loading.value = true;

    await EmployerUserApi.updatePhoto(id, payload)
      .then(() => {
        showNotif({
          message: "You have successfully uploaded your profile photo",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doUpdateAddress = async (id, payload) => {
    loading.value = true;

    await UserAddressApi.update(id, {
      ...payload,
    })
      .then(() => {})
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doCreateRequirements = async (payload) => {
    loading.value = true;

    await EmployerRequirementApi.create(payload)
      .then(() => {
        showNotif({
          message: "You have succesfully added information",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doUpdateRequirements = async (id, payload) => {
    loading.value = true;

    await EmployerRequirementApi.update(id, payload)
      .then(() => {
        showNotif({
          message: "You have succesfully updated your information",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doDeleteRequirement = async (payload) => {
    deleteLoading.value = true;

    await EmployerRequirementApi.delete(payload)
      .then(() => {
        showNotif({
          message: "You have succesfully deleted a record",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        showNotif({
          message: error?.response?.data?.message || "Invalid credentials.",
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        deleteLoading.value = false;
      });
  };

  return {
    loading,
    deleteLoading,
    doUpdateBasicInfo,
    doUpdateAddress,
    doUpdatePhoto,
    doCreateRequirements,
    doUpdateRequirements,
    doDeleteRequirement,
  };
}

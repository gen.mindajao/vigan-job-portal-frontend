import { defineStore } from "pinia";
import { UserAddressApi } from "src/hooks/api";

const defaultState = () => ({
  address: [],
});

export const useAddress = defineStore("address", {
  state: defaultState,

  getters: {},

  actions: {
    async showAddress(id) {
      await UserAddressApi.show(id)
        .then((response) => {
          this.address = response.data.data;
        })
        .catch(() => {
          this.address = [];
        });
    },
  },
});

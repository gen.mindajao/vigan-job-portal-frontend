import { defineStore } from "pinia";
import {
  EmployerUserApi,
  EmployerIndustryApi,
  EmployerRequirementApi,
  UserAddressApi,
} from "src/hooks/api";

const defaultState = () => ({
  userDetails: [],
  userDetail: null,
  industries: [],
  industry: null,
  requirements: [],
  requirement: null,
  address: [],
});

export const useEmployerList = defineStore("employer", {
  state: defaultState,

  getters: {},

  actions: {
    async showUser(id) {
      await EmployerUserApi.show(id)
        .then((response) => {
          this.userDetails = response.data.data;
        })
        .catch(() => {
          this.userDetails = [];
        });
    },

    async getEmployerIndustries() {
      await EmployerIndustryApi.get()
        .then((response) => {
          this.industries = response.data.data;
        })
        .catch(() => {
          this.industries = [];
        });
    },

    async showIndustry(id) {
      await EmployerIndustryApi.show(id)
        .then((response) => {
          this.industry = response.data.data;
        })
        .catch(() => {
          this.industry = null;
        });
    },

    async showAddress(id) {
      await UserAddressApi.show(id)
        .then((response) => {
          this.address = response.data.data;
        })
        .catch(() => {
          this.address = [];
        });
    },

    async getEmployerRequirements() {
      await EmployerRequirementApi.get()
        .then((response) => {
          this.requirements = response.data.data;
        })
        .catch(() => {
          this.requirements = [];
        });
    },

    async showEmployerRequirement(id) {
      await EmployerRequirementApi.show(id)
        .then((response) => {
          this.requirement = response.data.data;
        })
        .catch(() => {
          this.requirement = null;
        });
    },
  },
});

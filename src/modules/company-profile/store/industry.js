import { defineStore } from "pinia";
import { EmployerIndustryApi } from "src/hooks/api";

const defaultState = () => ({
  industries: [],
  industry: null,
});

export const useIndustry = defineStore("industry", {
  state: defaultState,

  getters: {},

  actions: {
    async getEmployerIndustries() {
      await EmployerIndustryApi.get()
        .then((response) => {
          this.industries = response.data.data;
        })
        .catch(() => {
          this.industries = [];
        });
    },

    async showEmployerIndustry(id) {
      await EmployerIndustryApi.show(id)
        .then((response) => {
          this.industry = response.data.data;
        })
        .catch(() => {
          this.industry = null;
        });
    },
  },
});

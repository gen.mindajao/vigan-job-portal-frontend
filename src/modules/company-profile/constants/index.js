export const COMPANY_PROFILE_BASE_PATH = "/company-profile";

export const COMPANY_PROFILE_ROUTE_NAMES = {
  CompanyProfile: {
    name: "CompanyProfilePage",
    link: `${COMPANY_PROFILE_BASE_PATH}/`,
  },
};

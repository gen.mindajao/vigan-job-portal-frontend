export const AUTH_ROUTE_NAMES = {
  Login: {
    name: "LoginPage",
    link: "/login",
  },
  Register: {
    name: "RegisterPage",
    link: "/register",
  },
  Onboarding: {
    name: "OnboardingPage",
    link: "/onboarding",
  },
  AccountSetup: {
    name: "AccountSetupPage",
    link: "/account-setup",
  },
  EmailVerification: {
    name: "EmailVerificationPage",
    link: "/email-verification",
  },
  OnboardingJobSeeker: {
    name: "OnboardingJobSeeker",
    link: "/onboarding-jobseeker",
  },
  OnboardingEmployer: {
    name: "OnboardingEmployer",
    link: "/onboarding-employer",
  },
  ApplicationStatus: {
    name: "ApplicationStatusPage",
    link: "/application-status",
  },
  ForgotPassword: {
    name: "ForgotPasswordPage",
    link: "/forgot-password",
  },
};

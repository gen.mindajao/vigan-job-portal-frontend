import { ref } from "vue";
import { UserAddressApi } from "src/hooks/api";

export default function useUserAddressHook() {
  const loading = ref(false);

  const doCreateAddress = async (id, payload = {}) => {
    loading.value = true;

    await UserAddressApi.create(id, {
      ...payload,
    })
      .then((response) => {})
      .catch((error) => {})
      .finally(() => {
        loading.value = false;
      });
  };

  return {
    loading,
    doCreateAddress,
  };
}

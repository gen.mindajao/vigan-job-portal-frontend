import { LocalStorage } from "quasar";
import { ref } from "vue";
import { AuthApi } from "src/hooks/api";
import { MY_JOBS_EMPLOYER_ROUTE_NAMES } from "src/modules/my-jobs-employer/constants";
import { COMPANY_PROFILE_ROUTE_NAMES } from "src/modules/company-profile/constants";
import { SEARCH_JOB_ROUTE_NAMES } from "src/modules/search-job/constants";
import { AUTH_ROUTE_NAMES } from "../constants";
import useNotification from "src/hooks/notification";
import usePageRouter from "src/hooks/pageRouter";
import { getErrorMessage } from "src/hooks/errorMessage";

export default function useAuthHook() {
  const loading = ref(false);
  const { showNotif } = useNotification();
  const { router } = usePageRouter();

  const doRedirectEmployerPortal = () => {
    router.push({ name: MY_JOBS_EMPLOYER_ROUTE_NAMES.MyJobsEmployer.name });
  };

  const doRedirectEmployerProfile = () => {
    router.push({ name: COMPANY_PROFILE_ROUTE_NAMES.CompanyProfile.name });
  };

  const doRedirectJobseekerPortal = () => {
    router.push({ name: SEARCH_JOB_ROUTE_NAMES.SearchJob.name });
  };

  const doRedirectApplicationStatus = () => {
    router.push({ name: AUTH_ROUTE_NAMES.ApplicationStatus.name });
  };

  const doLogin = async (payload) => {
    loading.value = true;

    await AuthApi.login(payload)
      .then((response) => {
        LocalStorage.set("role", response.data.data.user.user_type);
        LocalStorage.set("authenticated", true);
        LocalStorage.set("token", response.data.data.access_token);
        LocalStorage.set("auth", response.data.data.user);

        setTimeout(() => {
          if (response.data.data.user.user_type === "job-seeker") {
            doRedirectJobseekerPortal();
          } else if (response.data.data.user.user_type === "employer") {
            doAuthRedirect();
          }
        }, 1000);
      })
      .catch((error) => {
        // console.log("error: ", error.response.data.message);
        // "Account doesn't exist. Please create an account before logging in." ||
        //   "Invalid credentials.",
        showNotif({
          message: getErrorMessage(error),
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doRegisterEmployer = async (payload) => {
    loading.value = true;

    await AuthApi.registerEmployer(payload)
      .then((response) => {
        LocalStorage.set("role", "employer");
        LocalStorage.set("authenticated", true);
        LocalStorage.set("token", response.data.data.access_token);
        LocalStorage.set("auth", response.data.data.user);

        // setTimeout(() => {
        //   doRedirectApplicationStatus();
        // }, 1000);
      })
      .catch((error) => {
        LocalStorage.clear();

        showNotif({
          message: getErrorMessage(error),
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doRegisterJobseeker = async (payload) => {
    loading.value = true;

    await AuthApi.registerJobseeker(payload)
      .then((response) => {
        // showNotif({
        //   message: "Success!",
        //   color: "positive",
        //   position: "bottom-right",
        // });

        LocalStorage.set("role", "job-seeker");
        LocalStorage.set("authenticated", true);
        LocalStorage.set("token", response.data.data.token);
        LocalStorage.set("auth", response.data.data.user);

        setTimeout(() => {
          doRedirectApplicationStatus();
        }, 1000);
      })
      .catch((error) => {
        LocalStorage.clear();

        showNotif({
          message: getErrorMessage(error),
          color: "negative",
          position: "bottom-right",
        });
        loading.value = false;
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doVerifyEmail = async (payload) => {
    loading.value = true;

    await AuthApi.verifyEmail(payload)
      .then((response) => {
        LocalStorage.set("role", response.data.data.user.user_type);
        LocalStorage.set("authenticated", true);
        LocalStorage.set("token", response.data.data.access_token);
        LocalStorage.set("auth", response.data.data.user);

        setTimeout(() => {
          if (response.data.success === true) {
            router.push("onboarding-jobseeker");
          }
        }, 1000);
      })
      .catch((error) => {
        LocalStorage.clear();

        showNotif({
          message: getErrorMessage(error),
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doSendCode = async (payload) => {
    loading.value = true;

    await AuthApi.sendCode(payload)
      .then((response) => {
        LocalStorage.set("role", response.data.data.user.user_type);
        LocalStorage.set("authenticated", true);
        LocalStorage.set("token", response.data.data.access_token);
        LocalStorage.set("auth", response.data.data.user);
      })
      .catch((error) => {
        showNotif({
          message: getErrorMessage(error),
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doResendCode = async (payload) => {
    loading.value = true;

    let params = {
      email: LocalStorage.getItem("userForm")
        ? LocalStorage.getItem("userForm").email
        : "",
      mobile_number: LocalStorage.getItem("userForm")
        ? LocalStorage.getItem("userForm").mobile_number
        : "",
    };

    console.log("payload: ", payload);
    console.log("params: ", params);

    await AuthApi.resendCode(params)
      .then((response) => {
        console.log("response: ", response);
        // LocalStorage.set("role", response.data.data.user.user_type);
        // LocalStorage.set("authenticated", true);
        // LocalStorage.set("token", response.data.data.access_token);
        // LocalStorage.set("auth", response.data.data.user);

        showNotif({
          message: response?.data?.message || "Invalid credentials.",
          color: "positive",
          position: "bottom-right",
        });
      })
      .catch((error) => {
        console.log("error: ", error.response);
        showNotif({
          message: getErrorMessage(error),
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doCheckEmail = async (payload) => {
    loading.value = true;

    await AuthApi.checkEmail(payload)
      .then((response) => {
        LocalStorage.set("role", response.data.data.user.user_type);
        LocalStorage.set("authenticated", true);
        LocalStorage.set("token", response.data.data.access_token);
        LocalStorage.set("auth", response.data.data.user);

        setTimeout(() => {
          if (response.data.success === true) {
            router.push("onboarding-jobseeker");
          }
        }, 1000);
      })
      .catch((error) => {
        LocalStorage.clear();

        showNotif({
          message: getErrorMessage(error),
          color: "negative",
          position: "bottom-right",
        });
      })
      .finally(() => {
        loading.value = false;
      });
  };

  const doAuthRedirect = () => {
    const isAuthenticated = LocalStorage.getItem("auth").email_verified_at;

    if (isAuthenticated === null) {
      doRedirectEmployerProfile();
    } else {
      doRedirectEmployerPortal();
    }
  };

  return {
    loading,
    doRegisterEmployer,
    doRedirectEmployerPortal,
    doRedirectEmployerProfile,
    doLogin,
    doRegisterJobseeker,
    doRedirectJobseekerPortal,
    doRedirectApplicationStatus,
    doVerifyEmail,
    doSendCode,
    doResendCode,
    doCheckEmail,
    doAuthRedirect,
  };
}

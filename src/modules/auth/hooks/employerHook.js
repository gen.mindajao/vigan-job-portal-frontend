import { ref } from "vue";
import { EmployerIndustryApi, EmployerRequirementApi } from "src/hooks/api";

export default function useEmployerHook() {
  const loading = ref(false);

  const doCreateIndustry = async (payload) => {
    loading.value = true;

    await EmployerIndustryApi.create(payload)
      .then((response) => {})
      .catch((error) => {})
      .finally(() => {
        loading.value = false;
      });
  };

  const doCreateRequirements = async (payload) => {
    loading.value = true;

    await EmployerRequirementApi.create(payload)
      .then((response) => {})
      .catch((error) => {})
      .finally(() => {
        loading.value = false;
      });
  };

  return {
    loading,
    doCreateIndustry,
    doCreateRequirements,
  };
}

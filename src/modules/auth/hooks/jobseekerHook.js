import { ref } from "vue";
import {
  JobseekerCertificateApi,
  JobseeekerCharacterReferenceApi,
  JobseekerEducationApi,
  JobseekerSkillApi,
  JobseekerOccupationApi,
  JobseekerWorkHistoryApi,
} from "src/hooks/api";

export default function useJobseekerHook() {
  const loading = ref(false);

  const doCreateEducation = async (payload) => {
    loading.value = true;

    await JobseekerEducationApi.create(payload)
      .then((response) => {})
      .catch((error) => {})
      .finally(() => {
        loading.value = false;
      });
  };

  const doCreateSkill = async (payload) => {
    loading.value = true;

    await JobseekerSkillApi.create(payload)
      .then((response) => {})
      .catch((error) => {})
      .finally(() => {
        loading.value = false;
      });
  };

  const doCreateWork = async (payload) => {
    loading.value = true;

    await JobseekerWorkHistoryApi.create(payload)
      .then((response) => {})
      .catch((error) => {})
      .finally(() => {
        loading.value = false;
      });
  };

  const doCreateCertificate = async (payload) => {
    loading.value = true;

    await JobseekerCertificateApi.create(payload)
      .then((response) => {})
      .catch((error) => {})
      .finally(() => {
        loading.value = false;
      });
  };

  const doCreateCharacterReference = async (payload) => {
    loading.value = true;

    await JobseeekerCharacterReferenceApi.create(payload)
      .then((response) => {})
      .catch((error) => {})
      .finally(() => {
        loading.value = false;
      });
  };

  const doCreateOccupation = async (payload) => {
    loading.value = true;

    await JobseekerOccupationApi.create(payload)
      .then((response) => {})
      .catch((error) => {})
      .finally(() => {
        loading.value = false;
      });
  };

  return {
    loading,
    doCreateEducation,
    doCreateSkill,
    doCreateWork,
    doCreateCertificate,
    doCreateCharacterReference,
    doCreateOccupation,
  };
}

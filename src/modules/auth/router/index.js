import { AUTH_ROUTE_NAMES } from "../constants";

export const AuthModuleRoutes = [
  {
    path: AUTH_ROUTE_NAMES.Login.link,
    name: AUTH_ROUTE_NAMES.Login.name,
    components: {
      default: () =>
        import(
          /* webpackChunkName: "LoginPage" */ `../pages/${AUTH_ROUTE_NAMES.Login.name}.vue`
        ),
    },
    meta: {
      requiredAuth: false,
      viewFor: [],
      forceDefault: true,
    },
  },
  {
    path: AUTH_ROUTE_NAMES.Register.link,
    name: AUTH_ROUTE_NAMES.Register.name,
    components: {
      default: () =>
        import(
          /* webpackChunkName: "RegisterPage" */ `../pages/${AUTH_ROUTE_NAMES.Register.name}.vue`
        ),
    },
    meta: {
      requiredAuth: false,
      viewFor: [],
      forceDefault: true,
    },
  },
  {
    path: AUTH_ROUTE_NAMES.Onboarding.link,
    name: AUTH_ROUTE_NAMES.Onboarding.name,
    components: {
      default: () =>
        import(
          /* webpackChunkName: "OnboardingPage" */ `../pages/${AUTH_ROUTE_NAMES.Onboarding.name}.vue`
        ),
    },
    meta: {
      requiredAuth: false,
      viewFor: [],
      forceDefault: true,
    },
  },
  {
    path: AUTH_ROUTE_NAMES.AccountSetup.link,
    name: AUTH_ROUTE_NAMES.AccountSetup.name,
    components: {
      default: () =>
        import(
          /* webpackChunkName: "AccountSetupPage" */ `../pages/${AUTH_ROUTE_NAMES.AccountSetup.name}.vue`
        ),
    },
    meta: {
      requiredAuth: false,
      viewFor: [],
      forceDefault: true,
    },
  },
  {
    path: AUTH_ROUTE_NAMES.EmailVerification.link,
    name: AUTH_ROUTE_NAMES.EmailVerification.name,
    components: {
      default: () =>
        import(
          /* webpackChunkName: "EmailVerificationPage" */ `../pages/${AUTH_ROUTE_NAMES.EmailVerification.name}.vue`
        ),
    },
    meta: {
      requiredAuth: false,
      viewFor: [],
      forceDefault: true,
    },
  },
  {
    path: AUTH_ROUTE_NAMES.OnboardingJobSeeker.link,
    name: AUTH_ROUTE_NAMES.OnboardingJobSeeker.name,
    components: {
      default: () =>
        import(
          /* webpackChunkName: "OnboardingJobSeeker" */ `../pages/${AUTH_ROUTE_NAMES.OnboardingJobSeeker.name}.vue`
        ),
    },
    meta: {
      requiredAuth: false,
      viewFor: [],
      forceDefault: true,
    },
  },
  {
    path: AUTH_ROUTE_NAMES.OnboardingEmployer.link,
    name: AUTH_ROUTE_NAMES.OnboardingEmployer.name,
    components: {
      default: () =>
        import(
          /* webpackChunkName: "OnboardingEmployer" */ `../pages/${AUTH_ROUTE_NAMES.OnboardingEmployer.name}.vue`
        ),
    },
    meta: {
      requiredAuth: false,
      viewFor: [],
      forceDefault: true,
    },
  },
  {
    path: AUTH_ROUTE_NAMES.ApplicationStatus.link,
    name: AUTH_ROUTE_NAMES.ApplicationStatus.name,
    components: {
      default: () =>
        import(
          /* webpackChunkName: "ApplicationStatus" */ `../pages/${AUTH_ROUTE_NAMES.ApplicationStatus.name}.vue`
        ),
    },
    meta: {
      requiredAuth: false,
      viewFor: [],
      forceDefault: true,
    },
  },
  {
    path: AUTH_ROUTE_NAMES.ForgotPassword.link,
    name: AUTH_ROUTE_NAMES.ForgotPassword.name,
    components: {
      default: () =>
        import(
          /* webpackChunkName: "ForgotPassword" */ `../pages/${AUTH_ROUTE_NAMES.ForgotPassword.name}.vue`
        ),
    },
    meta: {
      requiredAuth: false,
      viewFor: [],
      forceDefault: true,
    },
  },
];

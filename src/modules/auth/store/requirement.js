import { defineStore } from "pinia";
import { EmployerRequirementApi } from "src/hooks/api";

const defaultState = () => ({
  requirements: [],
  requirement: null,
});

export const useRequirement = defineStore("requirements", {
  state: defaultState,

  getters: {},

  actions: {
    async getEmployerRequirements() {
      await EmployerRequirementApi.get()
        .then((response) => {
          this.requirements = response.data.data;
        })
        .catch(() => {
          this.requirements = [];
        });
    },

    async showEmployerRequirement(id) {
      await EmployerRequirementApi.show(id)
        .then((response) => {
          this.requirement = response.data.data;
        })
        .catch(() => {
          this.requirement = null;
        });
    },
  },
});

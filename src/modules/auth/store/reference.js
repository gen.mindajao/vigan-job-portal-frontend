import { defineStore } from "pinia";
import {
  CityApi,
  BarangayApi,
  EducationalLevelApi,
  IndustryApi,
  SkillApi,
  OccupationApi,
} from "src/hooks/api";
import { LocalStorage } from "quasar";

const defaultState = () => ({
  cities: LocalStorage.getItem("cities"),
  barangays: LocalStorage.getItem("barangays"),
  levels: LocalStorage.getItem("levels"),
  skills: LocalStorage.getItem("skills"),
  occupations: LocalStorage.getItem("occupations"),
  industries: LocalStorage.getItem("industries"),
});

export const useReference = defineStore("reference", {
  state: defaultState,

  getters: {},

  actions: {
    async getCities(payload) {
      const params = payload;

      await CityApi.get({
        params,
      })
        .then((response) => {
          this.cities = response.data.data;
          LocalStorage.set("cities", this.cities);
        })
        .catch(() => {
          this.cities = [];
        });
    },

    async getBarangays(payload) {
      const params = payload;

      await BarangayApi.get({
        params,
      })
        .then((response) => {
          this.barangays = response.data.data;
          LocalStorage.set("barangays", this.barangays);
        })
        .catch(() => {
          this.barangays = [];
        });
    },

    async getLevels(payload) {
      const params = payload;

      await EducationalLevelApi.get({
        params,
      })
        .then((response) => {
          this.levels = response.data.data;
          LocalStorage.set("levels", this.levels);
        })
        .catch(() => {
          this.levels = [];
        });
    },

    async getSkills(payload) {
      const params = payload;

      await SkillApi.get({
        params,
      })
        .then((response) => {
          this.skills = response.data.data;
          LocalStorage.set("skills", this.skills);
        })
        .catch(() => {
          this.skills = [];
        });
    },

    async getOccupations(payload) {
      const params = payload;

      await OccupationApi.get({
        params,
      })
        .then((response) => {
          this.occupations = response.data.data;
          LocalStorage.set("occupations", this.occupations);
        })
        .catch(() => {
          this.occupations = [];
        });
    },

    async getIndustries(payload) {
      const params = payload;

      await IndustryApi.get({
        params,
      })
        .then((response) => {
          this.industries = response.data.data;
          LocalStorage.set("industries", this.industries);
        })
        .catch(() => {
          this.industries = [];
        });
    },
  },
});

import { defineStore } from "pinia";
import { JobseekerWorkHistoryApi } from "src/hooks/api";

const defaultState = () => ({
  workHistories: [],
  workHistory: null,
});

export const useWorkHistory = defineStore("works", {
  state: defaultState,

  getters: {},

  actions: {
    async getWorkHistories() {
      await JobseekerWorkHistoryApi.get()
        .then((response) => {
          this.workHistories = response.data.data;
        })
        .catch(() => {
          this.workHistories = [];
        });
    },

    async showWorkHistory(id) {
      await JobseekerWorkHistoryApi.show(id)
        .then((response) => {
          this.workHistory = response.data.data;
        })
        .catch(() => {
          this.workHistory = null;
        });
    },
  },
});

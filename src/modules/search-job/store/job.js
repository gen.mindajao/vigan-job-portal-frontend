import { defineStore } from "pinia";
import { clearConfigCache } from "prettier";
import { JobseekerJobPostApi, JobseekerSavedJobPostApi } from "src/hooks/api";

const defaultState = () => ({
  jobs: [],
  job: null,
  savedJobs: [],
  jobListLoading: false,
  page: 1,
  isLoaded: false,
});

export const useJob = defineStore("jobs", {
  state: defaultState,

  getters: {},

  actions: {
    async getJobs(payload = {}) {
      if (this.page == 1) {
        this.jobListLoading = true;
      }
      const params = new URLSearchParams({
        page: this.page,
        per_page: 10,
      });

      if (payload.sortBy) {
        switch (payload.sortBy) {
          case 1:
            params.append("sort_by_most_recent", "desc");
            break;
          case 2:
            params.append("sort_by_job_title", "asc");
            break;
          case 3:
            params.append("sort_by_job_title", "desc");
            break;
          case 4:
            params.append("sort_by_company_name", "asc");
            break;
          case 5:
            params.append("sort_by_company_name", "desc");
            break;
        }
      }

      if (payload.search) {
        params.append("search", payload.search);
      }
      if (payload.date_posted) {
        params.append("date_posted", payload.date_posted);
      }
      if (payload.full_time) {
        params.append("full_time", payload.full_time);
      }
      if (payload.part_time) {
        params.append("part_time", payload.part_time);
      }
      if (payload.freelance_or_contract) {
        params.append("freelance_or_contract", payload.freelance_or_contract);
      }
      if (payload.entry_level) {
        params.append("entry_level", payload.entry_level);
      }
      if (payload.intermediate) {
        params.append("intermediate", payload.intermediate);
      }
      if (payload.expert) {
        params.append("expert", payload.expert);
      }
      if (payload.on_site) {
        params.append("on_site", payload.on_site);
      }
      if (payload.remote) {
        params.append("remote", payload.remote);
      }
      if (payload.hybrid) {
        params.append("hybrid", payload.hybrid);
      }

      return await JobseekerJobPostApi.get(params)
        .then((response) => {
          this.jobListLoading = false;
          response.data.data.data.forEach((value, index) => {
            let job = this.jobs.find((item) => item.id === value.id);

            if (!job) {
              this.jobs.push(value);
            }
          });

          this.sortJobs();

          if (this.page != response.data.data.last_page) {
            this.page += 1;
            this.isLoaded = false;
          } else {
            this.isLoaded = true;
          }
          return response.data.data;
        })
        .catch(() => {
          this.jobs = [];
        });
    },

    async getJob(id) {
      await JobseekerJobPostApi.show(id)
        .then((response) => {
          this.job = response.data.data;
        })
        .catch(() => {
          this.job = {};
        });
    },

    async getSavedJobs() {
      const params = new URLSearchParams();

      params.append("filter", "saved");

      await JobseekerSavedJobPostApi.get(params)
        .then((response) => {
          this.savedJobs = response.data.data;
        })
        .catch(() => {
          this.savedJobs = [];
        });
    },

    sortJobs() {
      this.jobs = this.jobs.sort(
        (a, b) => new Date(b.updated_at) - new Date(a.updated_at)
      );
    },

    resetJobs() {
      this.jobs = [];
      this.job = null;
      this.savedJobs = [];
      this.jobListLoading = false;
      this.page = 1;
      this.isLoaded = false;
    },
  },
});

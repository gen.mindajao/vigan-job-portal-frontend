import { defineStore } from "pinia";
import { JobseekerSearchApi } from "src/hooks/api";

const defaultState = () => ({
  searches: [],
  search: null,
});

export const useSearch = defineStore("searches", {
  state: defaultState,

  getters: {},

  actions: {
    async getSearches() {
      const params = new URLSearchParams({
        per_page: 3,
      });
      await JobseekerSearchApi.get(params)
        .then((response) => {
          this.searches = response.data.data;
        })
        .catch(() => {
          this.searches = {};
        });
    },
  },
});

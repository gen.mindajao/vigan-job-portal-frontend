import { SEARCH_JOB_BASE_PATH, SEARCH_JOB_ROUTE_NAMES } from "../constants";

export const SearchJobModuleRoutes = [
  {
    path: SEARCH_JOB_BASE_PATH,
    name: "SearchJobModule",
    component: () =>
      import(
        /* webpackChunkName: "SearchJobModule" */ "../SearchJobModule.vue"
      ),
    redirect: SEARCH_JOB_ROUTE_NAMES.SearchJob.link,
    meta: {
      requiredAuth: true,
    },
    children: [
      {
        path: SEARCH_JOB_ROUTE_NAMES.SearchJob.link,
        name: SEARCH_JOB_ROUTE_NAMES.SearchJob.name,
        components: {
          default: () =>
            import(
              /* webpackChunkName: "SearchJobPage" */ `../pages/${SEARCH_JOB_ROUTE_NAMES.SearchJob.name}.vue`
            ),
        },
        meta: {
          requiredAuth: true,
          viewFor: [],
          forceDefault: false,
        },
      },
    ],
  },
];

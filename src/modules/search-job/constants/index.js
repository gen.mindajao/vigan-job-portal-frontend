export const SEARCH_JOB_BASE_PATH = "/search-job";

export const SEARCH_JOB_ROUTE_NAMES = {
  SearchJob: {
    name: "SearchJobPage",
    link: `${SEARCH_JOB_BASE_PATH}/`,
  },
};

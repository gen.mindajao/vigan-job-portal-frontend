import { defineStore } from "pinia";
import { EmployerNotificationCountApi } from "src/hooks/api";
const defaultState = () => ({
  notificationCount: null,
  needToRefresh: false,
});

export const useEmployer = defineStore("employer", {
  state: defaultState,

  getters: {},

  actions: {
    async getNotificationCount() {
      console.log("getNotificationCount");
      await EmployerNotificationCountApi.get()
        .then((response) => {
          this.notificationCount = response.data.data;
        })
        .catch(() => {
          this.notificationCount = null;
        });
    },

    setRefresh(refresh) {
      console.log("setRefresh: ", refresh);
      this.needToRefresh = refresh;
    },
  },
});

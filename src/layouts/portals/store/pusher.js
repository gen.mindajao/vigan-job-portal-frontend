import { defineStore } from "pinia";

const defaultState = () => ({
  channels: [],
});

export const usePusher = defineStore("pusher", {
  state: defaultState,

  getters: {},

  actions: {
    addPusherChannel(channel) {
      this.channels.push(channel);
    },

    clearPusherChannel() {
      this.channels.splice(0, this.channels.length);
    },
  },
});

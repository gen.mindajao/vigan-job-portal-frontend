import { defineStore } from "pinia";
import { JobseekerUserApi } from "src/hooks/api";
import { EmployerUserApi } from "src/hooks/api";
import { LocalStorage } from "quasar";

const defaultState = () => ({
  userDetails: LocalStorage.getItem("auth"),
});

export const useUser = defineStore("users_" + LocalStorage.getItem("auth").id, {
  state: defaultState,

  getters: {
    isResumeUploaded(state) {
      if (state.userDetails) {
        if (state.userDetails.cv_or_resume !== null) {
          return "resume";
        }
      }
      return null;
    },
    isProfileUploaded(state) {
      if (state.userDetails) {
        if (state.userDetails.profile_picture !== null) {
          return "profile";
        }
      }
      return null;
    },
  },

  actions: {
    async getBasicInfo(id) {
      this.userDetails = LocalStorage.getItem("auth");
      await JobseekerUserApi.show(id)
        .then((response) => {
          this.userDetails = response.data.data;
          let data = LocalStorage.getItem("auth");
          data.profile_picture_url = this.userDetails.profile_picture_url;
          LocalStorage.set("auth", data);
        })
        .catch(() => {
          this.userDetails = [];
        });
    },

    async getEmployerInfo(id) {
      this.userDetails = LocalStorage.getItem("auth");
      await EmployerUserApi.show(id)
        .then((response) => {
          this.userDetails = response.data.data;
          let data = LocalStorage.getItem("auth");
          data.profile_picture_url = this.userDetails.profile_picture_url;
          LocalStorage.set("auth", data);
        })
        .catch(() => {
          this.userDetails = [];
        });
    },
  },
});

export const PORTAL_JOBSEEKER_MENU_OPTIONS = [
  {
    label: "Search Job",
    name: "SearchJobPage",
    link: "/search-job",
    isActive: true,
    meta: {
      roles: ["job-seeker"],
    },
  },
  {
    label: "Messages",
    name: "MessagesPage",
    link: "/messages",
    isActive: false,
    meta: {
      roles: ["job-seeker"],
    },
  },
  {
    label: "My Profile",
    name: "MyProfilePage",
    link: "/my-profile",
    isActive: false,
    meta: {
      roles: ["job-seeker"],
    },
  },
];

export const PORTAL_JOBSEEKER_MENU_OPTIONS_MOBILE = [
  {
    label: "Search Job",
    name: "SearchJobPage",
    link: "/search-job",
    isActive: true,
    meta: {
      roles: ["admin", "job-seeker", "developer"],
    },
  },
  {
    label: "My Jobs",
    name: "MyProfilePage",
    link: "/my-jobs",
    isActive: false,
    meta: {
      roles: ["admin", "job-seeker", "developer"],
    },
  },
  {
    label: "My Profile",
    name: "MyProfilePage",
    link: "/my-profile",
    isActive: false,
    meta: {
      roles: ["admin", "job-seeker", "developer"],
    },
  },
  {
    label: "Messages",
    name: "MessagesPage",
    link: "/messages",
    isActive: false,
    meta: {
      roles: ["admin", "job-seeker", "employer"],
    },
  },
  {
    label: "Setting",
    name: "MyProfilePage",
    link: "/settings-jobseeker",
    isActive: false,
    meta: {
      roles: ["admin", "job-seeker", "developer"],
    },
  },
];

export const PORTAL_EMPLOYER_MENU_OPTIONS = [
  {
    label: "My Jobs",
    name: "MyJobsEmployerPage",
    link: "/my-jobs-employer",
    isActive: false,
    meta: {
      roles: ["employer"],
    },
  },
  {
    label: "Messages",
    name: "MessagesEmployerPage",
    link: "/messages-employer",
    isActive: false,
    meta: {
      roles: ["employer"],
    },
  },
  {
    label: "Company Profile",
    name: "CompanyProfilePage",
    link: "/company-profile",
    isActive: false,
    meta: {
      roles: ["employer"],
    },
  },
];

export const GENDERS = [
  {
    name: "Male",
    id: "male",
  },
  {
    name: "Female",
    id: "female",
  },
];

export const DUMMY_SCHOOL_YEARS = [
  {
    name: "2002",
    id: "2002",
  },
  {
    name: "2001",
    id: "2001",
  },
  {
    name: "2000",
    id: "2000",
  },
  {
    name: "1999",
    id: "1999",
  },
  {
    name: "1998",
    id: "1998",
  },
  {
    name: "1997",
    id: "1997",
  },
  {
    name: "1996",
    id: "1996",
  },
  {
    name: "1995",
    id: "1995",
  },
  {
    name: "1994",
    id: "1994",
  },
  {
    name: "1993",
    id: "1993",
  },
  {
    name: "1992",
    id: "1992",
  },
  {
    name: "1991",
    id: "1991",
  },
  {
    name: "1990",
    id: "1990",
  },
];

import { boot } from "quasar/wrappers";
import Pusher from "pusher-js";

const pusherConfig = {
  APP_KEY: "44904fc410f8cf8b0c87",
  APP_SECRET: "ea6686a3fa7442a47097",
  APP_CLUSTER: "ap1",
};

const pusher = new Pusher(pusherConfig.APP_KEY, {
  cluster: pusherConfig.APP_CLUSTER,
});

export default boot(({ app }) => {
  pusher.connect();
  pusher.connection.bind("error", function (err) {
    console.log("error: ", err);
  });

  pusher.connection.bind("state_change", function (states) {
    // states = {previous: 'oldState', current: 'newState'}
    console.log("Connection current state is " + states.current);
    if ((states.current = "connected")) {
      console.log("socketID: ", pusher.connection.socket_id);
    }
  });

  app.config.globalProperties.$pusher = pusher;
});

export { pusher, pusherConfig };

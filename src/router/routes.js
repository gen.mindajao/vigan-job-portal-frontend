import { AuthModuleRoutes } from "src/modules/auth/router";
import { SearchJobModuleRoutes } from "src/modules/search-job/router";
import { MyJobsModuleRoutes } from "src/modules/my-jobs/router";
import { SavedJobsModuleRoutes } from "src/modules/saved-jobs/router";
import { MyProfileModuleRoutes } from "src/modules/my-profile/router";
import { MyJobsEmployerModuleRoutes } from "src/modules/my-jobs-employer/router";
import { MessagesEmployerModuleRoutes } from "src/modules/messages-employer/router";
import { CompanyProfileModuleRoutes } from "src/modules/company-profile/router";
import { MessagesModuleRoutes } from "src/modules/messages/router";
import { SettingsEmployerModuleRoutes } from "src/modules/settings-employer/router";
import { SettingsJobseekerModuleRoutes } from "src/modules/settings-jobseeker/router";

const routes = [
  {
    path: "/",
    component: () =>
      import(
        /* webpackChunkName: "JobseekerPortalLayout" */ "src/layouts/portals/JobseekerPortalLayout.vue"
      ),
    redirect: "/search-job",
    children: [
      ...SearchJobModuleRoutes,
      ...MyJobsModuleRoutes,
      ...MyProfileModuleRoutes,
      ...MessagesModuleRoutes,
      ...SavedJobsModuleRoutes,
      ...SettingsJobseekerModuleRoutes,
    ],
  },
  {
    path: "/my-jobs-employer",
    component: () =>
      import(
        /* webpackChunkName: "EmployerPortalLayout" */ "src/layouts/portals/EmployerPortalLayout.vue"
      ),
    redirect: "/my-jobs-employer",
    children: [
      ...MyJobsEmployerModuleRoutes,
      ...MessagesEmployerModuleRoutes,
      ...CompanyProfileModuleRoutes,
      ...SettingsEmployerModuleRoutes,
    ],
  },

  ...AuthModuleRoutes,

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
